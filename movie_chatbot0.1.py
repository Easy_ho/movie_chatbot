# -*- coding: utf-8 -*-
import re
import urllib.request

from bs4 import BeautifulSoup

from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter


SLACK_TOKEN = 'xoxb-675633564227-678315614962-kYzO50Ao6iZHHfS0h6dEnogv'
SLACK_SIGNING_SECRET = '5d62b1713e4212be72d3c6a0810ff4e5'

app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(
    SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)

# https://www.google.com/search?q=주변+영화관

# 크롤링 함수 구현하기
'''
def _crawl_locations_keywords(text):
    url_match1 = 'https://www.google.com/search?q=주변+영화관'
    source_code1 = urllib.request.urlopen(url_match1).read()
    soup1 = BeautifulSoup(source_code1, "html.parser")
    movie_locations = []

    for google_txt in soup1.find_all("div",class_="ccBEnf"):
        movie_locations.append(google_txt.get_text().strip().replace('\n',' ').replace('|',''))
        if len(movie_locations) >= 3:
            break
    return u'\n'.join(movie_locations)
'''
def _crawl_portal_keywords(text):

    url_match = 'https://movie.naver.com/movie/running/current.nhn'
    source_code = urllib.request.urlopen(url_match).read()
    soup = BeautifulSoup(source_code, "html.parser")
    keywords = []
    tickets= []
    score = []
    count=1
    count1=1
    for naver_txt in soup.find_all("dt", class_="tit"):
        keywords.append(str(count)+"위 : "+naver_txt.get_text().strip().replace('\n',' ').replace('|',''))
        count+=1
        if len(keywords) >= 10:
            break

    for naver_txt in soup.find_all("dl", class_="info_star"):
        score.append(naver_txt.get_text().strip().replace('\n','').replace('|','').replace('네티즌','네티즌 평점 : ').replace('참여',' 참여인원 :'))
        if len(score) >= 10:
            break

    for naver_txt in soup.find_all("dl", class_="info_exp"):
        tickets.append(naver_txt.get_text().strip().replace('\n','').replace('|','').replace('예매율','예매율 : '))
        if len(tickets) >= 10:
            break

    url_match1 ='https://movie.naver.com/movie/running/premovie.nhn?viewType=image&festival=N&order=open'
    source_code1 = urllib.request.urlopen(url_match1).read()
    soup1 = BeautifulSoup(source_code1, "html.parser")

    movie_preview=[]
    movie_data=[]

    for naver_txt in soup1.find_all("strong",class_="tit"):
        movie_preview.append(str(count1)+": "+naver_txt.get_text().strip().replace('\n',' ').replace('|',''))
        count1+=1
        if len(movie_preview) >= 10:
            break

    for naver_txt in soup1.find_all("span",class_="dsc"):
        movie_data.append(naver_txt.get_text().strip().replace('\n',' ').replace('|',''))
        if len(movie_data) >= 10:
            break

    previews=[]

    for i in range(len(movie_preview)):
        previews.append(movie_preview[i])
        previews.append(movie_data[i])

    print(previews)
    
    if "상영영화" in text :
        return u'\n'.join(keywords)
    elif "도움말" in text:
        return u'\n'.join(text)
    elif "개봉예정영화" in text:
        return u'\n'.join(previews)
    

    result=[]

    for i in range(len(keywords)):
        keyword_str = str(keywords[i])
        text_str = str(text)
        if text_str[13:] in keyword_str:
            result.append(tickets[i])
            result.append(score[i])
            return u' '.join(result)
            


# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]
    #location=_crawl_locations_keywords(text)
    keywords = _crawl_portal_keywords(text)
    if "네티즌" in keywords:
        slack_web_client.chat_postMessage(
            channel=channel,
            text=keywords
        )
    elif "도움말" in text:
        slack_web_client.chat_postMessage(
            channel=channel,
            text="'현재 상영영화' 를 입력하시면 상영중인 영화가 나와요!\n 상영중인 영화의 제목을 입력하시면 예매율과 평점이 나와요!\n 개봉예정영화를 입력하시면 앞으로 개봉예정 영화가 나와요!"
        )
    elif "개봉예정영화" in text:
        slack_web_client.chat_postMessage(
            channel=channel,
            text="*개봉 예정순으로 10개 보여드릴게요!*\n\n"+keywords
        )
    else:
        slack_web_client.chat_postMessage(
            channel=channel,
            text="*예매순으로 TOP 10 보여드릴게요!*\n\n" + keywords
        )

# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('0.0.0.0', port=5000)
